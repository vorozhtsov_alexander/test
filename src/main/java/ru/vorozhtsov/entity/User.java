package ru.vorozhtsov.entity;

import com.fasterxml.jackson.annotation.JsonView;
import ru.vorozhtsov.server.BaseEntity;
import ru.vorozhtsov.service.View;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "\"user\"" , indexes = {
        @Index(unique = true, name = "IDX_USER__EMAIL", columnList = "email"),
})
public class User extends BaseEntity {

    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    private static final String BALANCE = "balance";

    @JsonView(View.Public.class)
    private String email;
    private String password;
    @JsonView(View.Public.class)
    private BigDecimal balance = new BigDecimal(0);
    @JsonView(View.Public.class)
    private List<BalanceLog> logs = new ArrayList<>();

    public User() {
    }

    @Column(name = EMAIL, length = 100, nullable = false)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = PASSWORD, length = 50, nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = BALANCE, nullable = false)
    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    public List<BalanceLog> getLogs() {
        return logs;
    }

    public void setLogs(List<BalanceLog> logs) {
        this.logs = logs;
    }
}
