package ru.vorozhtsov.entity;

import com.fasterxml.jackson.annotation.JsonView;
import ru.vorozhtsov.server.BaseEntity;
import ru.vorozhtsov.service.View;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "\"balance_log\"" )
public class BalanceLog extends BaseEntity {

    private static final String USER_ID = "userId";
    private static final String CHANGE_TIME = "changeTime";
    private static final String CHANGE = "change";

    private User user;
    @JsonView(View.Public.class)
    private Date date;
    @JsonView(View.Public.class)
    private BigDecimal change;

    public BalanceLog() {
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = USER_ID, nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = CHANGE_TIME, nullable = false)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Column(name = CHANGE, nullable = false)
    public BigDecimal getChange() {
        return change;
    }

    public void setChange(BigDecimal change) {
        this.change = change;
    }
}
