package ru.vorozhtsov.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface BalanceLogRepository extends JpaRepository<BalanceLog, Long> {

}
