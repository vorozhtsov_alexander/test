package ru.vorozhtsov.service;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.vorozhtsov.entity.BalanceLog;
import ru.vorozhtsov.entity.BalanceLogRepository;
import ru.vorozhtsov.entity.User;
import ru.vorozhtsov.entity.UserRepository;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Date;

@RestController
@RequestMapping(value = "/api")
@Transactional
public class UserController {

    private final UserRepository userRepository;
    private final BalanceLogRepository balanceLogRepository;

    public UserController(UserRepository userRepository, BalanceLogRepository balanceLogRepository) {
        this.userRepository = userRepository;
        this.balanceLogRepository = balanceLogRepository;
    }

    @JsonView(View.Public.class)
    @PostMapping(value = "/signUp")
    public ResponseEntity signUp(@RequestBody() @Valid SignUpParameter parameters) {

        User user = userRepository.findByEmail(parameters.getEmail());

        if (user != null) {
            throw new RuntimeException("user exists already"); //todo friendly message
        }

        user = new User();
        user.setEmail(parameters.getEmail());
        user.setPassword(parameters.getPassword());

        userRepository.save(user); //todo catch exception related to IDX_USER__EMAIL index

        return ResponseEntity.ok(user);
    }

    @JsonView(View.Public.class)
    @PostMapping(value = "/deposit")
    public ResponseEntity deposit(@RequestBody() @Valid BalanceChangeParameter parameters) {
        return changeBalance(parameters, true);
    }

    @JsonView(View.Public.class)
    @PostMapping(value = "/withdraw")
    public ResponseEntity withdraw(@RequestBody() @Valid BalanceChangeParameter parameters) {
        return changeBalance(parameters, false);
    }

    @JsonView(View.Public.class)
    @PostMapping(value = "/balance")
    public ResponseEntity balance(@RequestBody() @Valid BalanceParameter parameters) {
        //todo use security instead. We should take user from session
        User user = userRepository.findByEmail(parameters.getEmail());

        if (user == null) {
            throw new RuntimeException("user doesn't exist"); //todo friendly message
        }

        user.getLogs().size();//todo init lazy loading

        return ResponseEntity.ok(user);
    }

    private ResponseEntity changeBalance(BalanceChangeParameter parameters, boolean deposit) {
        //todo use security instead. We should take user from session
        User user = userRepository.findByEmail(parameters.getEmail());

        if (user == null) {
            throw new RuntimeException("user doesn't exist"); //todo friendly message
        }

        BalanceLog log = new BalanceLog();
        log.setDate(new Date());
        if (deposit) {
            log.setChange(parameters.getMoney());
        } else {
            log.setChange(parameters.getMoney().multiply(new BigDecimal(-1)));
        }
        log.setUser(user);
        balanceLogRepository.save(log); //todo catch exception related to IDX_USER__EMAIL index

        if (deposit) {
            user.setBalance(user.getBalance().add(parameters.getMoney()));
        } else {
            user.setBalance(user.getBalance().subtract(parameters.getMoney())); //todo check credit limit
        }
        user.getLogs().add(log);
        userRepository.save(user);

        return ResponseEntity.ok(user);
    }
}
