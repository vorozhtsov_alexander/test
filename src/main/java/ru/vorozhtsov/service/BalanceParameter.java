package ru.vorozhtsov.service;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class BalanceParameter {

    public BalanceParameter() {
    }

    @NotEmpty
    @Size(max=100)
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}