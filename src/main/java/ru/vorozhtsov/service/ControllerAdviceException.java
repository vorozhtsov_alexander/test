package ru.vorozhtsov.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ControllerAdviceException {

    private static final Logger logger = LogManager.getLogger(ControllerAdviceException.class);

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<?> handleInvalidRequest(Exception e) {

        logger.error("internal error", e);

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
