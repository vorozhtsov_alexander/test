package ru.vorozhtsov.service;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class BalanceChangeParameter {

    public BalanceChangeParameter() {
    }

    @NotEmpty
    @Size(max=100)
    private String email;
    @Min(1)
    private BigDecimal money;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
}