package ru.vorozhtsov.service;

import org.springframework.beans.factory.annotation.Required;

import javax.validation.constraints.Size;
import javax.validation.constraints.NotEmpty;

public class SignUpParameter {

    public SignUpParameter() {
    }

    @NotEmpty
    @Size(max=100)
    private String email;
    @NotEmpty
    @Size(max=50)
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}