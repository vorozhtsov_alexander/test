package ru.vorozhtsov.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created  by alexander vorozhtsov.
 */
@Configuration
@EnableConfigurationProperties
public class ApplicationProperties {

}
