package ru.vorozhtsov;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import ru.vorozhtsov.entity.BalanceLogRepository;
import ru.vorozhtsov.entity.User;
import ru.vorozhtsov.entity.UserRepository;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicationTests {

	@Autowired
	private MockMvc mvc;
	@Autowired
	UserRepository userRepository;
	@Autowired
	BalanceLogRepository balanceLogRepository;

	@Test
	@Transactional
	public void baseTest() throws Exception {

		User user = userRepository.findByEmail("vorozhtsov.alexander@gmail.com");

		if (user != null) {
			user.getLogs().forEach(item -> balanceLogRepository.delete(item));
			userRepository.delete(user);
		}

		mvc.perform(post("/api/signUp")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"email\":\"vorozhtsov.alexander@gmail.com\", \"password\":\"password\" }")
                )
                .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());

		mvc.perform(post("/api/deposit")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"email\":\"vorozhtsov.alexander@gmail.com\", \"money\":\"100\" }")
		)
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());

		mvc.perform(post("/api/withdraw")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"email\":\"vorozhtsov.alexander@gmail.com\", \"money\":\"50\" }")
		)
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());

		mvc.perform(post("/api/balance")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"email\":\"vorozhtsov.alexander@gmail.com\"}")
		)
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());
	}
}
